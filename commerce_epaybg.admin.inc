<?php

define('COMMERCE_EPAYBG_ACTION_URL', 'https://epay.bg/');
define('COMMERCE_EPAYBG_ACTION_DEMO_URL', 'https://devep2.datamax.bg/ep2/epay2_demo/');

// url function is throwing fatal error so stopprd using it
//define('URL_OK', url('epaybg/success', array('absolute' => TRUE)));
define('URL_OK', $_SERVER['SERVER_NAME'].'epaybg/success');
define('URL_CANCEL', $_SERVER['SERVER_NAME'].'epaybg/fail');
define('URL_DONE', $_SERVER['SERVER_NAME'].'epaybg/result');

/**
 * Payment method callback: settings form.
 */
function commerce_epaybg_settings_form($settings = NULL) {
  //unset($settings["url_done"]);unset($settings["url_cancel"]);unset($settings["url_ok"]);

  // default values
  $settings = (array) $settings + array(
    'server' => COMMERCE_EPAYBG_ACTION_DEMO_URL,
    'page_type' => 'paylogin',
    'min' => '',
    'secret_key' => '',
    'email' => '',
    'url_ok' => URL_OK,
    'url_cancel' => URL_CANCEL,
    'url_done' => URL_DONE,
    'exp_date_hours' => '1440',
    'status' => 'pending',
    'success_message' => 'The paiment was succesful!',
    'fail_message' => 'The paiment failed!',

  );



  $form = array();

  $form['server'] = array(
    '#type' => 'radios',
    '#title' => t('ePay.bg server'),
    '#options' => array(
      COMMERCE_EPAYBG_ACTION_DEMO_URL => ('Test - use for testing.'),
      COMMERCE_EPAYBG_ACTION_URL => ('Live - use for processing real transactions'),
    ),
    '#default_value' => $settings['server'],
    '#required' => TRUE,
  );

  $form['page_type'] = array(
    '#type'           => 'select',
    '#title'          => t('Payment page type'),
    '#default_value'  => $settings['page_type'],
    '#description'    => t('Type of the page that a client will see when redirected to ePay.bg'),
    '#options'        => array(
                            'paylogin'          => t('Standard'),
                            'credit_paydirect'  => t('Direct Credit Card'),
                          ),
    '#required' => TRUE,
  );
  $form['min'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Client number'),
    '#default_value'  => $settings['min'],
    '#description'    => t("Merchant Identification Number (you must get this from your merchant account in ePay.bg)."),
    '#required' => TRUE,
  );
  $form['secret_key'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Secret key'),
    '#default_value'  => $settings['secret_key'],
    '#description'    => t("Secret key (you must get this from your merchant account in ePay.bg)."),
    '#required' => TRUE,
  );
  $form['email'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Email'),
    '#default_value'  => $settings['email'],
    '#description'    => t("Merchant email (as entered in your ePay.bg account)."),
    '#required' => TRUE,
  );
  $form['exp_date_hours'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Payment Expiration Time'),
    '#value'          => $settings['exp_date_hours'],
    '#description'    => t("After how many hours should the payment request expire. 1440 hours are 2 months."),
    '#required' => TRUE,
  );
  /*$form['url_ok'] = array(
    '#type'           => 'textfield',
    '#title'          => t('URL OK'),
    '#attributes'     => array('readonly' => 'TRUE'),
    '#default_value'  => $settings['url_ok'],
    '#description'    => t("_uid_ = user id of current account."),
    '#required' => TRUE,
  );
  $form['url_cancel'] = array(
    '#type'           => 'textfield',
    '#title'          => t('URL CANCEL'),
    '#attributes'     => array('readonly' => 'TRUE'),
    '#default_value'  => $settings['url_cancel'],
    '#description'    => t('Do not change this unless you know exactly what you are doing.'),
    '#required' => TRUE,
  );
  $form['url_done'] = array(
    '#type'           => 'textfield',
    '#title'          => t('URL DONE'),
    '#attributes'     => array('readonly' => 'TRUE'),
    '#value'          => $settings['url_done'],
    '#description'    => t("Paste this url in the field 'URL за известяване' in your ePay.bg merchant account."),
  );
  
  $form['currency'] = array(
    '#type'           => 'select',
    '#title'          => t('Currency'),
    '#options'        => array(
      'BGN' => 'BGN',
      'USD' => 'USD',
      'EUR' => 'EUR'
    ),
    '#default_value'  =>  $settings['currency'],
    '#description'    => t("Currency in ePay.bg system."),
    '#required' => TRUE,
  );
/*  $form['rate'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Rate to site currency'),
    '#default_value'  => variable_get('commerce_epaybg_rate', '1.00'),
    '#description'    => t("Please enter ePay.bg rate according to site currency."),
    '#required' => TRUE,
  );

  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#description' => t('Choose order status after customer sent the order'),
    '#options' => array('pending' => "Pending"),
    '#default_value' => $settings['status'],
    '#required' => TRUE,
  );
*/
   $form['success_message'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('Success message'),
    '#description' => t('Enter optional text that will be displayed when payment succesful'),
    '#default_value' => $settings['success_message'],
    '#required' => TRUE,
  );

  $form['fail_message'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('Fail message'),
    '#description' => t('Enter optional text that will be displayed when payment fail'),
    '#default_value' => $settings['fail_message'],
    '#required' => TRUE,
  );

  return $form;
}