<?php

/**
 * Define a replacement if the function hash_hmac() does not exist.
 */
function _get_hash_hmac() {
  $hash_hmac = 'hash_hmac';
  if (!function_exists($hash_hmac)) {
    $hash_hmac = 'hmac';
  }

  return $hash_hmac;
}
if (!function_exists('hash_hmac')) {
  function hmac($algo, $data, $passwd) {
    /* md5 and sha1 only */
    $algo = strtolower($algo);
    $p = array(
      'md5' => 'H32',
      'sha1' => 'H40',
    );
    if (strlen($passwd) > 64) {
      $passwd = pack($p[$algo], $algo($passwd));
    }
    if (strlen($passwd) < 64) {
      $passwd = str_pad($passwd, 64, chr(0));
    }

    $ipad = substr($passwd, 0, 64) ^ str_repeat(chr(0x36), 64);
    $opad = substr($passwd, 0, 64) ^ str_repeat(chr(0x5C), 64);

    return ($algo($opad . pack($p[$algo], $algo($ipad .$data))));
  }
}

/**
 * Decode the data comming from ePay.bg
 */

 function commerce_epaybg_decode($encoded) {
  $lines_arr = @split("\n", base64_decode($encoded));
  $data = array();

  if (empty($lines_arr[0])) {
    watchdog('commerce_epaybg', 'ePay.bg Error proccessing order data - cannot decode the encoded sting @encoded', array('@encoded' => $encoded), WATCHDOG_ERROR);

    return FALSE;
  }

  if (preg_match("/^INVOICE=(\d+):STATUS=(PAID|DENIED|EXPIRED)(:PAY_TIME=(\d+):STAN=(\d+):BCODE=([0-9a-zA-Z]+))?$/", $lines_arr[0], $regs) && !empty($regs)) {
    $data["order_id"]  = (int)$regs[1];
    $data["status"]   = $regs[2];
    $data["pay_time"]   = $regs[4];
    $data["bcode"]   = (int)$regs[6];

    if (!(int)$data["order_id"]) {
      watchdog('commerce_epaybg', 'ePay.bg Error proccessing order data - order_id @order_id is not an integer for the string @str generated from @encoded', array('@order_id' => $data["order_id"], '@str' => $lines_arr[0], '@encoded' => $encoded), WATCHDOG_ERROR);

      return FALSE;

    } else if ($data["status"] != "PAID") {
      watchdog('commerce_epaybg', 'ePay.bg order @order wasn\'t paid correctly - has status @status', array('@order_id' => $data["order_id"], "@status" => $data["status"]), WATCHDOG_ERROR);

      return FALSE;
    }

    return $data;

  } else {
    watchdog('commerce_epaybg', 'ePay.bg Error proccessing order data - cannot match the string @str generated from @encoded', array('@str' => $lines_arr[0], '@encoded' => $encoded), WATCHDOG_ERROR);

    return FALSE;
  }
 }


 /**
 * Creates an epaybg payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $epay_data
 *   An array of parameters, received fro epay
 */
function commerce_epaybg_create_transaction($order, $payment_method, $epay_data) {

  if (empty($epay_data["bcode"])) {
    watchdog('commerce_epaybg', 'No remote id(bcode) - data @data', array('@data' => $epay_data), WATCHDOG_ERROR);
    return FALSE;
  }

  // check if this remote id already exists
  $result = db_select('commerce_payment_transaction', 'pt')
    ->fields('pt')
    ->condition('pt.remote_id', $epay_data["bcode"])
    ->execute()
    ->fetchAssoc();

  if (!empty($result)) {
    watchdog('commerce_epaybg', 'Duplicate remote id(bcode) - data @data', array('@data' => $epay_data), WATCHDOG_ERROR);
    return FALSE;
  }

  $transaction = commerce_payment_transaction_new('commerce_epaybg', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];

  $wrapper = entity_metadata_wrapper('commerce_order', $order);

  $currency_code = $wrapper->commerce_order_total->currency_code->value();
  $amount = $wrapper->commerce_order_total->amount->value();

  $transaction->remote_id = $epay_data["bcode"];
  $transaction->amount = $amount;
  $transaction->currency_code = $currency_code;
  $transaction->payload[REQUEST_TIME] = $epay_data["pay_time"];

  // Set the transaction's statuses based on the IPN's payment_status.
  $transaction->remote_status = $epay_data["status"];

  // If we didn't get an approval response code...
  switch ($epay_data["status"]) {
    case 'EXPIRED':
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t("The payment has expired.");
      break;

    case 'DENIED':
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('The payment has denied.');
      break;

    case 'PAID':
      $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
      $transaction->message = t('The payment has completed.');
      break;

    default:
      $transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
      $transaction->message = t('An unexpected error occured.');
      break;
  }

  commerce_payment_transaction_save($transaction);
  
  rules_invoke_all('commerce_checkout_complete', $order);

  watchdog('commerce_epaybg', 'Transaction processed for Order @order_number with ID @remote_id.', array('@remote_id' => $epay_data["bcode"], '@order_number' => $order->order_number), WATCHDOG_INFO);

}