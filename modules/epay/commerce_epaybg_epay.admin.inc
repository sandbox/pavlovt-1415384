<?php

/**
 * Payment method callback: settings form.
 */
function commerce_epaybg_epay_settings_form($settings = NULL) {
  //unset($settings["url_done"]);unset($settings["url_cancel"]);unset($settings["url_ok"]);

  // default values
  $settings = (array) $settings + array(
    'server' => variable_get('COMMERCE_EPAYBG_ACTION_DEMO_URL'),
    'page_type' => 'paylogin',
    'min' => '',
    'secret_key' => '',
    'email' => '',
    'exp_date_hours' => '1440',
    'success_message' => 'The paiment was succesful!',
    'fail_message' => 'The paiment failed!',

  );



  $form = array();

  $form['page_type'] = array(
    '#type' => 'hidden',
    '#default_value' => $settings['page_type'],
  );

  $form['server'] = array(
    '#type' => 'radios',
    '#title' => t('ePay.bg server'),
    '#options' => array(
      variable_get('COMMERCE_EPAYBG_ACTION_DEMO_URL') => ('Test - use for testing.'),
      variable_get('COMMERCE_EPAYBG_ACTION_URL') => ('Live - use for processing real transactions'),
    ),
    '#default_value' => $settings['server'],
    '#required' => TRUE,
  );
  $form['min'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Client number'),
    '#default_value'  => $settings['min'],
    '#description'    => t("Merchant Identification Number (you must get this from your merchant account in ePay.bg)."),
    '#required' => TRUE,
  );
  $form['secret_key'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Secret key'),
    '#default_value'  => $settings['secret_key'],
    '#description'    => t("Secret key (you must get this from your merchant account in ePay.bg)."),
    '#required' => TRUE,
  );
  $form['email'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Email'),
    '#default_value'  => $settings['email'],
    '#description'    => t("Merchant email (as entered in your ePay.bg account)."),
    '#required' => TRUE,
  );
  $form['exp_date_hours'] = array(
    '#type'           => 'textfield',
    '#title'          => t('Payment Expiration Time'),
    '#value'          => $settings['exp_date_hours'],
    '#description'    => t("After how many hours should the payment request expire. 1440 hours are 2 months."),
    '#required' => TRUE,
  );
  $form['success_message'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('Success message'),
    '#description' => t('Enter optional text that will be displayed when payment succesful'),
    '#default_value' => $settings['success_message'],
    '#required' => TRUE,
  );

  $form['fail_message'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('Fail message'),
    '#description' => t('Enter optional text that will be displayed when payment fail'),
    '#default_value' => $settings['fail_message'],
    '#required' => TRUE,
  );

  return $form;
}